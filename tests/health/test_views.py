from django.urls import reverse
from rest_framework.test import APIClient


class TestHealthView:
    health_url = reverse("health:health")

    def test_health_view(self, api_client: APIClient) -> None:
        """
        Test to see if the /health endpoint returns a 200 response
        """
        response = api_client.get(self.health_url)

        assert response.status_code == 200
