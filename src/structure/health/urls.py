from django.urls import path

from .views import HealthViewSet

app_name = "health"

urlpatterns = [
    path("", HealthViewSet.as_view({"get": "simple"}), name="health"),
]
