#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main() -> None:
    """Run administrative tasks."""

    # Remove the current directory from path
    DIRNAME = os.path.dirname(os.path.abspath(__file__))
    sys.path.remove(DIRNAME)

    # Insert the parent directory on path
    PARENT_DIR = os.path.abspath(os.path.join(DIRNAME, ".."))
    sys.path.insert(0, PARENT_DIR)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "structure.config.settings")

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

        current_path = os.path.dirname(os.path.abspath(__file__))
        sys.path.append(os.path.join(current_path, "src"))

    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
