from django.apps import AppConfig


class HealthConfig(AppConfig):
    name = "structure.health"
    label = "health"
