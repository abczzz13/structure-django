clean:
		docker-compose down --remove-orphans -v

run:
		docker-compose up -d app

shell: run
		# ... shell_plus --print-sql


kill:
		docker-compose kill
		docker-compose down

build:
		docker-compose build app

build-tox:
		docker-compose build tox

pull_cache:
		docker-compose pull app

pull_tox_cache:
		docker-compose pull tox

push: push_tox
		docker-compose push app

push_tox:
		docker-compose push tox

pre-commit:
		pre-commit run --all-files

test:
		pytest --isort --flake8 --black --cov=src
		# pytest --cov-config=coverage.ini --isort --flake8 --black --cov=src

tox: kill build-tox
		docker-compose run --rm tox

tox/%: kill build-tox
		docker-compose run --rm --service-ports tox --test tests/$*
