from invoke import task


@task
def wait_for(ctx, host, timeout=30):  # type: ignore
    ctx.run(f"wait-for-it {host} --timeout={timeout}")


@task
def migrate(ctx):  # type: ignore
    """Django manage.py migration as Invoke task"""
    ctx.run("python manage.py migrate")


@task
def requirements(ctx):  # type: ignore
    """Install requirements with Poetry"""
    ctx.run("poetry install")


@task
def runserver(ctx, host="127.0.0.1", port=8000):  # type: ignore
    """Runserver as Invoke task"""
    task = "runserver"
    command = f"python manage.py {task} {host}:{port}"

    print(command)
    ctx.run(command)


@task
def test(ctx, test="", wait=True):  # type: ignore
    """Tox testing as Invoke task"""
    # command = "tox -c tox.ini"
    command = "tox -c ../../tox.ini"
    # command = "ls ../../root/tox.ini"
    if wait:
        wait_for(ctx, "postgres:5432")

    args = [command, test]
    ctx.run(" ".join(args), pty=True)


@task
def testpy(ctx, wait=True):  # type: ignore
    """Pytest testing as Invoke task"""
    command = "pytest -c"
    tox_file = "../tox.ini"
    coverage_file = "../coverage.ini"
    plugins = [
        "--isort",
        "--flake8",
        "--black",
        "--cov=app",
    ]
    ctx.run(f"{command} {tox_file} --cov-config={coverage_file} {' '.join(plugins)}")
