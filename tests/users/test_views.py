import random

import pytest
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User


@pytest.mark.django_db
class TestJWTTokens:
    token_url = reverse("token_obtain_pair")
    verify_url = reverse("token_verify")
    refresh_url = reverse("token_refresh")

    username = "testuser"
    email = "testuser@testcompany.com"
    password = "averygoodpasswordthatislongenough"

    user = None

    def test_get_token(self, api_client: APIClient) -> None:
        """
        Test to check if a valid JWT token can be obtained with valid
        credentials
        """
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        data = {
            "username": self.username,
            "password": self.password,
        }
        response = api_client.post(self.token_url, data)

        assert response.status_code == 200

        data = {"token": response.data["access"]}
        response = api_client.post(self.verify_url, data)

        assert response.status_code == 200

    def test_get_token_invalid_credentials(self, api_client: APIClient) -> None:
        """
        Test to check if it's possible to obtain a valid token with
        invalid credentials
        """
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        data = {
            "username": self.username,
            "password": "anotherpassword",
        }
        response = api_client.post(self.token_url, data)

        assert response.status_code == 401

        data = {
            "username": "anotheruser",
            "password": self.password,
        }
        response = api_client.post(self.token_url, data)

        assert response.status_code == 401

    def test_refresh_token(self, api_client: APIClient) -> None:
        """
        Test to check if refreshing tokens work
        """
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        data = {
            "username": self.username,
            "password": self.password,
        }
        response = api_client.post(self.token_url, data)

        data = {"refresh": response.data["refresh"]}
        response = api_client.post(self.refresh_url, data)

        assert response.status_code == 200

        data = {"token": response.data["access"]}
        response = api_client.post(self.verify_url, data)

        assert response.status_code == 200

    def test_refresh_token_invalid_token(self, api_client: APIClient) -> None:
        """
        Test to check if it's possible to get a access token with an
        invalid refresh token
        """
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        data = {
            "username": self.username,
            "password": self.password,
        }
        response = api_client.post(self.token_url, data)

        refresh_token = response.data["refresh"]
        temp_list = list(refresh_token)
        random.shuffle(temp_list)
        invalid_token = "".join(temp_list)

        data = {"refresh": invalid_token}
        response = api_client.post(self.refresh_url, data)

        assert response.status_code == 401
