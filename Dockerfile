FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

ENV PIP_VERSION 22.2.2
ENV POETRY_VERSION 1.2.1
ENV INVOKE_VERSION 1.7.3
ENV VIRTUALENV_VERSION 20.16.5

ARG APP_HOME=/app

WORKDIR ${APP_HOME}

RUN addgroup --system service \
    && adduser --system --ingroup service service

RUN pip install --upgrade \
      pip==${PIP_VERSION} \
      poetry==${POETRY_VERSION} \
      invoke==${INVOKE_VERSION} \
      virtualenv==${VIRTUALENV_VERSION} \
      && mkdir /venv \
      && virtualenv /venv \
      && apt update && apt install --no-install-recommends -y \
      build-essential libpq-dev wait-for-it \
      && apt purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
      && rm -rf /var/lib/apt/lists/*

COPY ./poetry.lock ${APP_HOME}/.
COPY ./pyproject.toml ${APP_HOME}/.
RUN . /venv/bin/activate \
      && poetry install --no-root --no-interaction -vvv

ENV PATH=/venv/bin:$PATH
ENV VIRTUAL_ENV=/venv/

COPY ./src/. ${APP_HOME}

RUN chown service:service ${APP_HOME}
USER service

WORKDIR structure
